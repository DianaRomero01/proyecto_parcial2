




function sendRequest(){

    var formData = {
        name: $('#exampleFirstName').val().trim(),
        lastName: $('#exampleLastName').val().trim(),
        email: $('#exampleInputEmail').val().trim(),
        password: $('#exampleInputPassword').val().trim()
    };

    var repeatPassword = $('#exampleRepeatPassword').val().trim();

    // check all inputs
    var isFormOk = true;
    $("#registerForm input") 
        .each(function () { 
            var empty = false;
            if($(this).prop('required')){
                empty = ($(this).val() == "" || $(this).val() == null);
            }
            if (empty) {
                alert("All fields is required");
                this.focus();
                isFormOk = false;
                return false;
            }
    }); 

    if(!isFormOk)
        return;

    if (formData.password != repeatPassword)
    {
        alert('Passwords doesn\'t match!');
        return;
    }


    $('#registerButton1').hide();
    $('#registerButton2').hide();
    $('#registerButton3').hide();

    $.ajax({
        type: 'POST',
        url: '/register',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(formData),
        statusCode: {
            404: function() {
                alert('Servidor no encontrado.');
            },
            500: function() {
                alert('Error interno del servidor.');
            },
        },
        success: function(response) { 
            alert(response.message);   
            window.location.href = "/login"
        },
        error: function(response) {
            console.log("Hubo un error en la respuesta");
        },
        complete: function(response){
            $('#registerButton1').show();
            $('#registerButton2').show();
            $('#registerButton3').show();
        }
    });

    
}