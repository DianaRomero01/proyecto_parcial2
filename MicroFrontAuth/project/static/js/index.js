var main = function () {
    "use strict";

    var addCommentFromInputBox = function () {
        var $new_comment;
        var emptyMsg = $(".comments #emptyMsg");

        if (emptyMsg.length > 0) {
            emptyMsg.remove();
        }

        if ($(".comment-input input").val() !== "") {
            submitComment();
            $new_comment = $("<p>").text($(".comment-input input").val());
            $new_comment.hide();
            $(".comments").append($new_comment);
            $new_comment.fadeIn();
            $(".comment-input input").val("");
        }
    };

    $(".comment-input button").on("click", function (event) {
        addCommentFromInputBox();
    });

    $(".comment-input input").on("keypress", function (event) {
        if (event.keyCode === 13) {
            addCommentFromInputBox();
        }
    });
};

$(document).ready(main);


function submitComment(){
    let commentText = document.getElementById('commentText').value;
    const sendPost = async () => {
        const url = '/comment'; // the URL to send the HTTP request to
        const body = `{"commentText":"${commentText}"}`; // whatever you want to send in the body of the HTTP request
        const headers = {'Content-Type': 'application/json'}; // if you're sending JSON to the server
        const method = 'POST';
        const response = await fetch(url, { method, body, headers });
        const data = await response.text(); // or response.json() if your server returns JSON
    }
    
    sendPost();
}