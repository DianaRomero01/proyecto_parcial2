document.addEventListener("DOMContentLoaded", function () {
    // Hacer la solicitud GET al servicio Spring Boot
    fetch('http://localhost:8081/api/faqs')
        .then(response => response.json())
        .then(data => {
            console.log("Data:", data);
            // Procesar datos y llenar el HTML
            const faqList = document.getElementById('faqList');
            
            let faqBubbleCount = 0;  // Contador para faq-bubble
            let faqBubbleOCount = 0;  // Contador para faq-bubbleO

            data.forEach((faq, index) => {
                const faqItem = document.createElement('li');
                faqItem.classList.add('faq-item');

                const faqBubble = document.createElement('div');
                faqBubble.classList.add(faqBubbleCount < 4 ? 'faq-bubble' : 'faq-bubbleO');

                const faqArrow = document.createElement('div');
                faqArrow.classList.add('faq-arrow');
                faqBubble.appendChild(faqArrow);

                const faqQuestion = document.createElement('h2');
                faqQuestion.classList.add('faq-question');
                faqQuestion.textContent = faq.question;
                faqQuestion.onclick = function () {
                    toggleAnswer(this);
                };

                const faqAnswer = document.createElement('p');
                faqAnswer.classList.add('faq-answer');
                faqAnswer.textContent = faq.answer;

                faqBubble.appendChild(faqQuestion);
                faqBubble.appendChild(faqAnswer);
                faqItem.appendChild(faqBubble);

                faqList.appendChild(faqItem);

                // Actualizar el contador correspondiente
                if (faqBubbleCount < 4) {
                    faqBubbleCount++;
                } else {
                    faqBubbleOCount++;
                }
            });
        })
        .catch(error => {
            console.error('Error al obtener las FAQs:', error);
        });
});




// Usand DOM para mostrar mas preguntas y luego menos
// Variable para llevar el seguimiento de las preguntas visibles
let visibleQuestions = 4; // Inicialmente, mostrar las primeras 4 preguntas
let buttonState = 'more'; // Estado inicial del botón

function toggleQuestions() {
    const questions = document.querySelectorAll('.faq-bubbleO');
    const button = document.getElementById('showMore');
    
    let i = 0;
    if (buttonState === 'more'){

        while (i < visibleQuestions && i < questions.length) {
            questions[i].style.display = 'block'; // 
            i++;
        }
    }

    if (buttonState === 'less'){

        while (i < visibleQuestions && i < questions.length) {
            questions[i].style.display = 'none'; // 
            i++;
        }
    }
    // Cambiar el estado del botón y su texto
    if (buttonState === 'more') {
        buttonState = 'less';
        button.innerText = 'Mostrar menos preguntas';
        visibleQuestions += 4;
    } else {
        buttonState = 'more';
        button.innerText = 'Mostrar más preguntas';
        visibleQuestions -= 4; // Ocultar 4 preguntas finales
    }
    
}


// JavaScript for toggling answers
function toggleAnswer(question) {
    console.log("Function called"); 
    var answer = question.nextElementSibling;
    if (answer.style.display === 'block') {
        answer.style.display = 'none';
    } else {
        answer.style.display = 'block';
    }
}
