// Agregar el evento de clic al botón de búsqueda
document.querySelector('#searchButton').addEventListener('click', searchAlbums);

function searchAlbums() {
  // Obtener el texto de la barra de búsqueda
  const searchTerm = document.querySelector('#busquedaB').value;

  // Crear la URL de la petición al microservicio
  const url = `http://localhost:8081/api/albums/search?title=${searchTerm}`;

  // Realizar la petición AJAX
  $.ajax({
    url,
    type: 'GET',
    success: (data) => {
      // Procesar los resultados
      const albums = data;
      // Borrar el contenido del elemento HTML #results
      const resultsElement = document.querySelector('#results');
      resultsElement.innerHTML = '';
      // Agregar los nombres de los resultados de la búsqueda a la lista HTML
      for (const album of albums) {
        const li = document.createElement('li');
        li.textContent = album.title; // Ajusta esto según la estructura de tu entidad Album
        document.querySelector('#results').appendChild(li);
      }
    },
    error: (error) => {
      // Manejar el error
      console.error('Error al buscar álbumes:', error);
    },
  });
}



// Llamada a la función de búsqueda para álbumes más destacados
searchTopAlbums('amo', '#topAlbums');

// Llamada a la función de búsqueda para álbumes más recientes
searchTopAlbums('nobody', '#recentAlbums');

// Función de búsqueda genérica para álbumes destacados y recientes
function searchTopAlbums(searchTerm, targetElementId) {
  // Crear la URL de la petición al microservicio
  const url = `http://localhost:8081/api/albums/search?title=${searchTerm}`;

  // Realizar la petición AJAX
  $.ajax({
    url,
    type: 'GET',
    success: (data) => {
      // Procesar los resultados
      const albums = data;

      // Tomar solo las 4 primeras respuestas
      const firstFourAlbums = albums.slice(0, 4);

      // Borrar el contenido del elemento HTML específico
      const resultsElement = document.querySelector(targetElementId);
      resultsElement.innerHTML = '';

      // Agregar los resultados de la búsqueda a la lista HTML
      for (const album of firstFourAlbums) {
        const cuadroDiv = document.createElement('div');
        cuadroDiv.classList.add('cuadro');

        const img = document.createElement('img');
        img.src = "{{url_for('static',filename='img/vinyldisk.png')}}";
        img.alt = `Portada del álbum ${album.title}`;

        const pTitulo = document.createElement('p');
        pTitulo.classList.add('descripcion');
        pTitulo.textContent = album.title;

        const pArtista = document.createElement('p');
        pArtista.classList.add('descripcion');
        pArtista.textContent = album.artist; // Ajusta esto según la estructura de tu entidad Album

        const btnCarro = document.createElement('button');
        btnCarro.classList.add('btn-carro');
        btnCarro.textContent = 'Añadir al Carro';

        cuadroDiv.appendChild(img);
        cuadroDiv.appendChild(pTitulo);
        cuadroDiv.appendChild(pArtista);
        cuadroDiv.appendChild(btnCarro);

        resultsElement.appendChild(cuadroDiv);
      }
    },
    error: (error) => {
      // Manejar el error
      console.error(`Error al buscar álbumes ${searchTerm}:`, error);
    },
  });
}


// Agregar el evento de clic al botón de búsqueda
document.querySelector('#searchButton').addEventListener('click', searchAlbums);
function toggleMobileMenu() {
    const mobileMenu = document.querySelector(".mobile-menu");
    mobileMenu.classList.toggle("active");
  }  