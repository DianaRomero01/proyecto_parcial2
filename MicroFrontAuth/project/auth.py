from flask import Blueprint, render_template, redirect, url_for, request, flash, jsonify
from flask_login import login_user, logout_user, login_required
from werkzeug.security import generate_password_hash, check_password_hash
from .models import User
from . import db
import time


auth = Blueprint('auth', __name__)

@auth.route('/login')
def login():
    return render_template('login.html', title="C-Vynil - Login")

@auth.route('/login', methods=['POST'])
def login_post():
    email    = request.form.get('email')
    password = request.form.get('password')
    remember = True if request.form.get('remember') else False

    user = User.query.filter_by(email=email).first()

    # check if the user actually exists
    # take the user-supplied password, hash it, and compare it to the hashed password in the database
    if not user or not check_password_hash(user.password, password):
        flash('Please check your login details and try again.')
        return redirect(url_for('auth.login')) # if the user doesn't exist or password is wrong, reload the page
    # if the above check passes, then we know the user has the right credentials
    login_user(user, remember=remember)
    # if the above check passes, then we know the user has the right credentials
    return redirect(url_for('main.home'))

@auth.route('/')
def register():
    return render_template('register.html',title="C-Vynil - Register")

@auth.route('/register', methods=['POST'])
def register_post():
    time.sleep(3)
    data = request.json

    name = data['name']
    lastName = data['lastName']
    email = data['email']
    password = data['password']

    response = {  
            "status" : "OK", 
            "message" : "Usuario registrado satisfactoriamente", 
        } 
    
    user = User.query.filter_by(email=email).first()

    if user: # if a user is found, we want to redirect back to signup page so user can try again
        response["message"] = "Email address already exists";
    else:
        # create a new user with the form data. Hash the password so the plaintext version isn't saved.
        new_user = User(email=email, name=name, lastName=lastName,password=generate_password_hash(password, method='sha256'))

        # add the new user to the database
        db.session.add(new_user)
        db.session.commit()

    return jsonify(response) 


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('auth.login'))