from flask import Blueprint, render_template, request
from flask_login import login_required, current_user
from .models import Comment
from . import db

main = Blueprint('main', __name__)

@main.route('/forgotpwd')
def forgotpwd():
    return render_template('forgotpwd.html')

@main.route('/home')
@login_required
def home():
    return render_template('home.html')

@main.route('/faq')
@login_required
def faq():
    return render_template('faq.html')

@main.route('/carrito')
@login_required
def carrito():
    return render_template('carrito.html')