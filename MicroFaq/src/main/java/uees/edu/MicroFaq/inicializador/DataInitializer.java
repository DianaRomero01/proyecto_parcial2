package uees.edu.MicroFaq.inicializador;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import uees.edu.MicroFaq.entidad.Faq;
import uees.edu.MicroFaq.repositorio.RepositorioFaq;


@Component
public class DataInitializer implements CommandLineRunner {

    private final RepositorioFaq repositorioFaq;

    public DataInitializer(RepositorioFaq repositorioFaq) {
        this.repositorioFaq = repositorioFaq;
    }

    @Override
    public void run(String... args) {
        // Verificar si ya existen preguntas y respuestas
        if (repositorioFaq.count() == 0) {
            // Insertar preguntas y respuestas solo si no hay datos
            Faq faq1 = new Faq();
            faq1.setQuestion("¿Qué tipos de vinilos personalizan en su tienda?");
            faq1.setAnswer("En nuestra tienda, ofrecemos la personalización de una variedad de vinilos, incluyendo vinilos decorativos para paredes, vinilos para vehículos, vinilos para laptops y otros dispositivos electrónicos, así como vinilos para ventanas.");
            repositorioFaq.save(faq1);

            Faq faq2 = new Faq();
            faq2.setQuestion("¿Cuánto tiempo se tarda en completar una personalización de vinilo?");
            faq2.setAnswer("El tiempo de producción puede variar según la complejidad del diseño y la cantidad de vinilos a personalizar. Por lo general, proporcionamos un plazo estimado al momento de realizar el pedido.");
            repositorioFaq.save(faq2);

            Faq faq3 = new Faq();
            faq3.setQuestion("¿Cuál es la durabilidad de los vinilos personalizados?");
            faq3.setAnswer("La durabilidad de los vinilos personalizados depende del tipo de vinilo utilizado y del entorno en el que se coloquen. Ofrecemos opciones de vinilos de alta calidad que son resistentes al desgaste y a la exposición al sol y al agua.");
            repositorioFaq.save(faq3);

            Faq faq4 = new Faq();
            faq4.setQuestion("¿Cuáles son sus políticas de devolución?");
            faq4.setAnswer("Tenemos una política de devolución que permite devoluciones en un plazo determinado si el producto no cumple con tus expectativas o si hay algún problema de calidad.");
            repositorioFaq.save(faq4);

            Faq faq5 = new Faq();
            faq5.setQuestion("¿Donde se encuentra ubicada la tienda?");
            faq5.setAnswer("Somos una tienda en linea, no tenemos una tienda fisica.");
            repositorioFaq.save(faq5);

            Faq faq6 = new Faq();
            faq6.setQuestion("¿De que ciudad o pais son?");
            faq6.setAnswer("Estamos en Guayaquil, Ecuador");
            repositorioFaq.save(faq6);

            Faq faq7 = new Faq();
            faq7.setQuestion("¿Hacen envios al exterior?");
            faq7.setAnswer("Actualmente solo hacemos envios dentro del pais.");
            repositorioFaq.save(faq7);

            Faq faq8 = new Faq();
            faq8.setQuestion("¿Que metodos de pago aceptan?");
            faq8.setAnswer("Aceptamos transferencias bancarias y tarjeta de credito/debito.");
            repositorioFaq.save(faq8);

            Faq faq9 = new Faq();
            faq9.setQuestion("¿Cual es el costo del envio a Provincias?");
            faq9.setAnswer("El costo de envio de 0.1kg hasta 2kg es de US$6,31. Por cada kilo adicional US$0.90");
            repositorioFaq.save(faq9);
        }
    }
}