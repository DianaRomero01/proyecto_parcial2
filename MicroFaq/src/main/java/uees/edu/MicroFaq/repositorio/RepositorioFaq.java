package uees.edu.MicroFaq.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import uees.edu.MicroFaq.entidad.Faq;

public interface RepositorioFaq extends JpaRepository<Faq, Long>{

}
