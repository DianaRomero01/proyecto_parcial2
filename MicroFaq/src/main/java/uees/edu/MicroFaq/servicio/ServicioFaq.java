package uees.edu.MicroFaq.servicio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import uees.edu.MicroFaq.entidad.Faq;
import uees.edu.MicroFaq.repositorio.RepositorioFaq;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ServicioFaq {

    @Autowired
    private RepositorioFaq repositorioFaq;

    public List<Faq> getAllFaqs() {
        return repositorioFaq.findAll();
    }

    public Faq getFaqById(Long id) {
        return repositorioFaq.findById(id).orElse(null);
    }

    public Faq saveFaq(Faq faq) {
        return repositorioFaq.save(faq);
    }

}
