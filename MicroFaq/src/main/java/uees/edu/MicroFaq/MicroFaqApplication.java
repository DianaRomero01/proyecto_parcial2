package uees.edu.MicroFaq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroFaqApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroFaqApplication.class, args);
	}

}
