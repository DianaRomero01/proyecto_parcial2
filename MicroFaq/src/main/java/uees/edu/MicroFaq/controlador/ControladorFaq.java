package uees.edu.MicroFaq.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uees.edu.MicroFaq.entidad.Faq;
import uees.edu.MicroFaq.servicio.ServicioFaq;

import java.util.List;

@CrossOrigin(origins = "http://localhost:5000")
@RestController
@RequestMapping("/api/faqs")
public class ControladorFaq {
    @Autowired
    private ServicioFaq servicioFaq;

    @GetMapping
    public List<Faq> getAllFaqs() {
        return servicioFaq.getAllFaqs();
    }

    @GetMapping("/{id}")
    public Faq getFaqById(@PathVariable Long id) {
        return servicioFaq.getFaqById(id);
    }

}
