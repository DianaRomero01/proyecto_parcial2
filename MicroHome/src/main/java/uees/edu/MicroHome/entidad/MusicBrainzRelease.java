package uees.edu.MicroHome.entidad;

import com.fasterxml.jackson.annotation.JsonProperty;


public class MusicBrainzRelease {

    private String title;

    @JsonProperty("artist-credit")
    private MusicBrainzArtistCredit artistCredit;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public MusicBrainzArtistCredit getArtistCredit() {
        return artistCredit;
    }

    public void setArtistCredit(MusicBrainzArtistCredit artistCredit) {
        this.artistCredit = artistCredit;
    }
}