package uees.edu.MicroHome.entidad;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class MusicBrainzResponse {

    @JsonProperty("releases")
    private List<MusicBrainzRelease> releases;

    public List<MusicBrainzRelease> getReleases() {
        return releases;
    }

    public void setReleases(List<MusicBrainzRelease> releases) {
        this.releases = releases;
    }
}