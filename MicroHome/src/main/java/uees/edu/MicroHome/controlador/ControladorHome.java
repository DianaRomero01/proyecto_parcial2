package uees.edu.MicroHome.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uees.edu.MicroHome.entidad.Album;
import uees.edu.MicroHome.servicio.ServicioHome;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;

@CrossOrigin(origins = "http://localhost:5000")
@RestController
@RequestMapping("/api/albums")
public class ControladorHome {

    @Autowired
    private ServicioHome albumService;

    @GetMapping
    public ResponseEntity<List<Album>> buscarAlbumsPorNombre(@RequestParam String title) {
        List<Album> albums = albumService.buscarAlbumsPorNombre(title);
        return ResponseEntity.ok(albums);
    }

    // Otros métodos del controlador según sea necesario
}