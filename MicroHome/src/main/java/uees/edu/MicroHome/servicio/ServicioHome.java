package uees.edu.MicroHome.servicio;


import uees.edu.MicroHome.entidad.Album;

import uees.edu.MicroHome.entidad.MusicBrainzRelease;
import uees.edu.MicroHome.entidad.MusicBrainzSearchResponse;
import uees.edu.MicroHome.repositorio.RepositorioHome;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import java.util.stream.Collectors;


@Service
public class ServicioHome {

    @Autowired
    private RepositorioHome albumRepository;

    private static final String MUSICBRAINZ_API_URL =
            "https://musicbrainz.org/ws/2/release/?query=release:%s&fmt=json";

    public List<Album> buscarAlbumsPorNombre(String title) {
        List<Album> albumsFromDB = albumRepository.findByTitleContainingIgnoreCase(title);
        if (!albumsFromDB.isEmpty()) {
            return albumsFromDB;
        }

        // Si no se encuentran en la base de datos, realizar una solicitud a MusicBrainz API
        RestTemplate restTemplate = new RestTemplate();
        String formattedUrl = String.format(MUSICBRAINZ_API_URL, title);
        ResponseEntity<MusicBrainzSearchResponse> responseEntity = restTemplate.getForEntity(
                formattedUrl, MusicBrainzSearchResponse.class);

        MusicBrainzSearchResponse musicBrainzResponse = responseEntity.getBody();

        // Procesar la respuesta y mapearla a tus entidades
        List<Album> albums = musicBrainzResponse.getReleases().stream()
                .map(this::mapMusicBrainzReleaseToAlbum)
                .collect(Collectors.toList());

        // Guardar los álbumes en la base de datos local
        albumRepository.saveAll(albums);

        return albums;
    }

    private Album mapMusicBrainzReleaseToAlbum(MusicBrainzRelease release) {
        Album album = new Album();
        album.setTitle(release.getTitle());
        album.setArtist(release.getArtistCredit().getName());
        // Mapear otros campos según sea necesario
        return album;
    }

    // Otros métodos del servicio según sea necesario
}