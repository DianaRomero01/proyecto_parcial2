package uees.edu.MicroHome;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroHomeApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroHomeApplication.class, args);
	}

}
