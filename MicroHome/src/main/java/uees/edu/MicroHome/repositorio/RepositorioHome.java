package uees.edu.MicroHome.repositorio;

import uees.edu.MicroHome.entidad.Album;
import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;

public interface RepositorioHome extends JpaRepository<Album, Long> {

    List<Album> findByTitleContainingIgnoreCase(String title);
}